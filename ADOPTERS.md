# The Good Docs Project Adopters

To add your organization's name to the list of Good Docs Project Adopters, follow the instructions in the [Adopters](https://gitlab.com/tgdp/templates/-/blob/main/ADOPTERS.md) guide in the templates repository.
